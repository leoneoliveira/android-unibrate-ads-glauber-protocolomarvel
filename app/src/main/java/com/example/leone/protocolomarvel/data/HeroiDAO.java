package com.example.leone.protocolomarvel.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.leone.protocolomarvel.model.Heroi;

import java.util.ArrayList;
import java.util.List;


public class HeroiDAO {

    HeroiDbHelper mHelper;

    public HeroiDAO(Context ctx){
        mHelper = new HeroiDbHelper(ctx);
    }

    public void inserir(Heroi heroi) {
        SQLiteDatabase db = mHelper.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(HeroiDbHelper.NOME         , heroi.nome);
        values.put(HeroiDbHelper.ANO_APARICAO , heroi.ano_aparicao);
        values.put(HeroiDbHelper.NACIONALIDADE, heroi.nacionalidade);
        values.put(HeroiDbHelper.RACA         , heroi.raca);
        values.put(HeroiDbHelper.STATUS       , heroi.status);
        values.put(HeroiDbHelper.IMG          , heroi.img);

        long id = db.insert(HeroiDbHelper.TABELA_HEROI, null, values);
        if (id == -1){
            throw  new RuntimeException("Erro ao inserir registro");
        }
        db.close();

    }

    public void excluir(Heroi heroi) {
        SQLiteDatabase db = mHelper.getWritableDatabase();
        db.delete(HeroiDbHelper.TABELA_HEROI,
                HeroiDbHelper.NOME + " = ? AND " +
                        HeroiDbHelper.ANO_APARICAO + " = ? ",
                new String[]{heroi.nome, String.valueOf(heroi.ano_aparicao)});
        db.close();

    }

    public List<Heroi> listar() {
        SQLiteDatabase db = mHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(
                "SELECT * FROM " + HeroiDbHelper.TABELA_HEROI + " ORDER BY " + HeroiDbHelper.NOME,
                null);

        int idx_nome   = cursor.getColumnIndex(HeroiDbHelper.NOME);
        int idx_ano    = cursor.getColumnIndex(HeroiDbHelper.ANO_APARICAO);
        int idx_raca   = cursor.getColumnIndex(HeroiDbHelper.RACA);
        int idx_nacio  = cursor.getColumnIndex(HeroiDbHelper.NACIONALIDADE);
        int idx_status = cursor.getColumnIndex(HeroiDbHelper.STATUS);
        int idx_img    = cursor.getColumnIndex(HeroiDbHelper.IMG);

        List<Heroi> herois = new ArrayList<>();
        while (cursor.moveToNext()) {

            String nome   = cursor.getString(idx_nome);
            int ano       = cursor.getInt(idx_ano);
            String raca   = cursor.getString(idx_raca);
            String nacio  = cursor.getString(idx_nacio);
            String status = cursor.getString(idx_status);
            String img    = cursor.getString(idx_img);


            Heroi heroi = new Heroi(nome,ano,nacio,raca,status,img);
            herois.add(heroi);
        }
        cursor.close();
        db.close();
        return herois;
    }

    public boolean isFavoritos(Heroi heroi) {
        SQLiteDatabase db = mHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(
                "SELECT id FROM " + HeroiDbHelper.TABELA_HEROI
                        + " WHERE " + HeroiDbHelper.NOME + " = ? AND "
                        + HeroiDbHelper.ANO_APARICAO + " = ? ",
                new String[]{heroi.nome, String.valueOf(heroi.ano_aparicao)});
        boolean existe = cursor.getCount() > 0;
        db.close();
        return existe;
    }
}
