package com.example.leone.protocolomarvel;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.leone.protocolomarvel.model.Categoria;
import com.example.leone.protocolomarvel.model.Heroi;
import com.example.leone.protocolomarvel.model.Marvel;
import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.util.ArrayList;
import java.util.List;


public class ListaHeroiFragment extends ListFragment implements SwipeRefreshLayout.OnRefreshListener {

    List<Heroi> mHeroi;
    HeroiAdpter mAdapter;
    SwipeRefreshLayout mSwipe;
    HeroiTask mTask;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mHeroi = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lista_herois, null);
        mSwipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        mSwipe.setOnRefreshListener(this);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (mHeroi.isEmpty()) {
            mAdapter = new HeroiAdpter(getActivity(), mHeroi);
            setListAdapter(mAdapter);
            carregarHerois();
        }

    }

    private void carregarHerois() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {

            if (mTask == null) {
                mTask = new HeroiTask();
                mTask.execute();
                mSwipe.setRefreshing(true);
            }else{
                mSwipe.setRefreshing(false);
            }
        } else {
            mSwipe.setRefreshing(false);
            Toast.makeText(getActivity(), R.string.msg_sem_conexao, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Heroi heroi = mHeroi.get(position);
        exibirItem(heroi);

    }

    public void exibirItem(Heroi heroi){
        if (getActivity() instanceof AoClicarNoHeroi) {
            ((AoClicarNoHeroi) getActivity()).clicouNoHeroi(heroi);
        }
    }



    @Override
    public void onRefresh() {
        carregarHerois();
    }



    class HeroiTask extends AsyncTask<Void, Void, Marvel> {

        @Override
        protected Marvel doInBackground(Void... params) {
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url("https://dl.dropboxusercontent.com/s/8q2lhllzkpsy5yw/marvel.json")
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
                String s = response.body().string();
                Gson gson = new Gson();
                Marvel marvel = gson.fromJson(s, Marvel.class);
                return marvel;

            } catch (Throwable e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Marvel marvel) {
            super.onPostExecute(marvel);
            if (marvel != null && marvel.categorias != null) {
                mHeroi.clear();
                for (Categoria categoria : marvel.categorias) {
                    mHeroi.addAll(categoria.heroi);
                }
                mAdapter.notifyDataSetChanged();

                // Carrega o primeiro item da lista de for tablet
                if (getResources().getBoolean(R.bool.tablet)) {
                    exibirItem(mHeroi.get(0));
                }
            } else {
                Toast.makeText(getActivity(), R.string.msg_erro_geral, Toast.LENGTH_SHORT).show();
            }
            mSwipe.setRefreshing(false);
        }
    }
}

