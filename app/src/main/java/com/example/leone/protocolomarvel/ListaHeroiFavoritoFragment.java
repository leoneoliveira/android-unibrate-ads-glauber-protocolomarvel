package com.example.leone.protocolomarvel;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.leone.protocolomarvel.data.HeroiDAO;
import com.example.leone.protocolomarvel.model.Categoria;
import com.example.leone.protocolomarvel.model.Heroi;
import com.example.leone.protocolomarvel.model.Marvel;
import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;


public class ListaHeroiFavoritoFragment extends ListFragment {

    List<Heroi> mHeroi;
    HeroiAdpter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mHeroi = new ArrayList<>();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Bus bus = ((HeroiApp)getActivity().getApplication()).getBus();
        bus.register(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Bus bus = ((HeroiApp)getActivity().getApplication()).getBus();
        bus.unregister(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (mHeroi.isEmpty()) {
            mAdapter = new HeroiAdpter(getActivity(), mHeroi);
            carregarHerois();
            setListAdapter(mAdapter);
        }

    }


    private void carregarHerois() {
        HeroiDAO dao = new HeroiDAO(getActivity());
        mHeroi.clear();
        mHeroi.addAll(dao.listar());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Heroi heroi = mHeroi.get(position);
        if (getActivity() instanceof AoClicarNoHeroi) {
            ((AoClicarNoHeroi) getActivity()).clicouNoHeroi(heroi);
        }
    }

    @Subscribe
    public void listaDeHeroiAtualizada(Heroi heroi){
        carregarHerois();
    }

}

