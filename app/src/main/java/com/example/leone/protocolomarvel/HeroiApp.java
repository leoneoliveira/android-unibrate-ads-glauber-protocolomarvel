package com.example.leone.protocolomarvel;

import android.app.Application;

import com.squareup.otto.Bus;

public class HeroiApp extends Application{

    Bus  mBus;

    @Override
    public void onCreate() {
        super.onCreate();
        mBus = new Bus();
    }

    public  Bus getBus(){
        return mBus;
    }
}
