package com.example.leone.protocolomarvel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.leone.protocolomarvel.model.Heroi;

public class DetalheHeroiActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_heroi);

        if (savedInstanceState == null) {
            Heroi heroi = (Heroi) getIntent().getSerializableExtra("heroi");
            DetalheHeroiFragment dhf = DetalheHeroiFragment.novaInstancia(heroi);
            getSupportFragmentManager().
                    beginTransaction().
                    replace(R.id.detalhe, dhf).
                    commit();
        }

    }
}
